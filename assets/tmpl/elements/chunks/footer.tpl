<footer class="footer" itemtype="http://schema.org/WPFooter">
    <div class="wrap">
    <div class="container-fluid">
        <div>&copy; GuitarNotes <span itemprop="copyrightYear">2018</span></div>
        <div class="crtext">Права на песни принадлежат их законным авторам.
            Все тексты взяты из открытых источников и представлены в учебных целях</div>
        <div class="liveinternet">
            <!--LiveInternet counter--><script type="text/javascript">
                document.write("<a href='//www.liveinternet.ru/click' "+
                        "target=_blank><img src='//counter.yadro.ru/hit?t23.11;r"+
                        escape(document.referrer)+((typeof(screen)=="undefined")?"":
                        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                        ";"+Math.random()+
                        "' alt='' title='LiveInternet: показано число посетителей за"+
                        " сегодня' "+
                        "border='0' width='88' height='15'><\/a>")
            </script><!--/LiveInternet-->
        </div>
    </div>
    </div>
</footer>
<link href="https://fonts.googleapis.com/css?family=Kelly+Slab|PT+Sans+Narrow&amp;subset=cyrillic" rel="stylesheet">
<script src="[[!ReplaceHttpToHttps? &link=[[++site_url]] ]]assets/tmpl/js/app.min.js"></script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
