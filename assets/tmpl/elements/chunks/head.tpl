<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="/assets/tmpl/css/style.min.css?time=3" type="text/css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta name="description" content="[[*metadescription]]">
    <meta name="keywords" content="[[*metakeywords]]">
    <meta name="google-site-verification" content="XxFJ4eEC11-b-tAyNaG6MUrNgW9azec5qxTQoO6kr1o" />

    <!--AB929B8298927C50CE0CFA5CD54BC20D-->
    <title>[[!If?
        &subject=`[[*metatitle]]`
        &operator=`!empty`
        &then=`[[*metatitle]]`
        &else=`[[*pagetitle]]`
        ]]</title>
</head>
