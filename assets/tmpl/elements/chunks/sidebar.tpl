<div class="col-lg-3">
    <aside class="sidebar">
        <div class="armodule">
            <h3>Популярные исполнители</h3>
            <ul>
                [[!pdoResources?
                    &depth=`1`
                    &parents=`0`
                    &includeTVs=`HitsPage`
                    &processTVs=`1`
                    &templates=`10`
                    &sortbyTV=`HitsPage`
                    &sortbyTVType=`integer`
                    &sortdirTV=`DESC`
                    &tpl=`simpResList`
                    &tvPrefix=``
                ]]

            </ul>

        </div>
    </aside>
</div>