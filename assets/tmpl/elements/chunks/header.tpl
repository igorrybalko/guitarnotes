<header class="header">
    <div class="topheader">
        <div class="wrap">
        <div class="container-fluid">
            <div class="logo">
                <a href="/"></a>
            </div>
            <div class="logoname"><a href="/"></a></div>
            <div class="search">
                [[!SimpleSearchForm? &landing=`61` &tpl=`searchFormChunk`]]
            </div>
        </div>
        </div>
    </div>
    <nav class="topmenu">
        <div class="container">
            <ul class="nav">
                [[pdoResources? &parents=`0` &resources=`77, 84` &tpl=`topMenuList`]]
            </ul>
        </div>
    </nav>
    <div class="wrap">
    <div class="container-fluid">
        <div class="alphwr">
            [[pdoMenu?
            &parents=`0`
            &level=`1`
            &outerClass=`alphabet`
            &scheme=`full`
            &tpl=`alphabetMenuElement`
            ]]
        </div>
    </div>
    </div>
</header>


