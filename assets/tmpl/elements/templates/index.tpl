<!doctype html>
<html lang="ru">
[[$head]]
<body>
<div class="glob">
    [[$header]]
    <main role="main">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="armodule">
                                <h3>Популярные композиции</h3>
                                <ul>
                                    [[!pdoResources?
                                        &parents=`0`
                                        &includeTVs=`HitsPage`
                                        &processTVs=`1`
                                        &templates=`11`
                                        &sortbyTV=`HitsPage`
                                        &sortbyTVType=`integer`
                                        &sortdirTV=`DESC`
                                        &tpl=`songsWith`
                                        &tvPrefix=``
                                    ]]
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="armodule">
                                <h3>Новинки</h3>
                                <ul>
                                    [[!pdoResources?
                                    &depth=`1`
                                    &parents=`0`
                                    &templates=`11`
                                    &sortby=`publishedon`
                                    &tpl=`songsWith`
                                    ]]
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
    </main>
</div>
[[$footer]]
</body>
</html>