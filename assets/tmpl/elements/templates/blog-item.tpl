<!doctype html>
<html lang="ru">
[[$head]]
<body class="tpl-blog-item">
<div class="glob">
    [[$header]]
    <main role="main">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <h1 itemprop="headline">[[*pagetitle]]</h1>
                    <div class="content">[[*content]]</div>
                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
    </main>
</div>
[[$footer]]
</body>
</html>