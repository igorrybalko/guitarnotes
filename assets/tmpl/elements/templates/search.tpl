<!doctype html>
<html lang="ru">
[[$head]]
<body>
<div class="glob">
    [[$header]]
    <main role="main">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <h1>Результаты поиска</h1>
                    <div class="searchresult">
                        [[!SimpleSearch]]
                    </div>
                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
    </main>
</div>
[[$footer]]
</body>
</html>