<!doctype html>
<html lang="ru">
[[$head]]
<body>

<div class="glob">
    [[$header]]

    <main role="main">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">

                    <h1 itemprop="headline">Исполнители на букву «[[*pagetitle]]»</h1>

                    <div class="tablecat">

                        <table class="table-striped">
                            <thead>
                            <tr>
                                <th class="td-img-cat"></th>
                                <th>Исполнитель</th>
                                <th class="songs-count">Композиций</th>
                            </tr>
                            </thead>
                            <tbody>
                                [[!pdoResources?
                                &tpl=`artistsList`
                                &depth=`0`
                                &sortby=`pagetitle`
                                &sortdir=`ASC`
                                &tvPrefix=``
                                &includeTVs=`tv_preimg_subcat`]]
                            </tbody>
                        </table>
                    </div>

                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
    </main>
</div>
[[$footer]]
</body>
</html>