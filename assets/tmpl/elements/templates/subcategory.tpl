<!doctype html>
<html lang="ru">
[[$head]]
<body>
<div class="glob">
    [[$header]]
    <main role="main" id="artistPage">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <!-- guitar aft art -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9489202619963673"
                         data-ad-slot="3687478435"
                         data-ad-format="auto"></ins>
                    <h1 itemprop="headline">[[*pagetitle]]</h1>
                    <div class="descgroupwr">
                        [[!If?
                        &subject=`[[*subcat_img]]`
                        &operator=`!empty`
                        &then=`
                            <div class="imggr">
                                <img src="/[[*subcat_img]]" alt="[[*pagetitle]]">
                            </div>
                        `]]

                        <div class="descgr">
                            [[*tv_description]]
                        </div>

                    </div>
                    <div class="tablesubcat">
                        <table class="table-striped" id="staticSongList">
                            <thead>
                            <tr>
                                <th>Композиции</th>
                                <th></th>
                                <th class="viewscount">Просмотры</th>
                            </tr>
                            </thead>
                            <tbody>
                                [[!pdoResources?
                                &tpl=`songsList`
                                &depth=`0`
                                &limit=`0`
                                &sortby=`pagetitle`
                                &sortdir=`ASC`
                                &tvPrefix=``
                                &includeTVs=`HitsPage`]]
                            </tbody>
                        </table>
                        <songs-list-comp :songs="songs"></songs-list-comp>
                    </div>
                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
        <script ref="songListVueData" type="application/json">
                [[!pdoResources?
                    &tpl=`songListVueData`
                    &depth=`0`
                    &limit=`0`
                    &sortby=`pagetitle`
                    &sortdir=`ASC`
                    &tvPrefix=``
                    &includeTVs=`HitsPage`]]
        </script>
    </main>
</div>
[[$footer]]

<script>
    (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="hitscount hidden">[[!HitsPage? &saveTv=`true`]]</div>
</body>
</html>