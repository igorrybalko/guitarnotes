<!doctype html>
<html lang="ru">
[[$head]]
<body class="tpl-article">
<div class="glob">
    [[$header]]
    <main role="main" id="articlePage">
        <div class="wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="gblock">
                            <!-- guitar aft art -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-9489202619963673"
                                 data-ad-slot="3687478435"
                                 data-ad-format="auto"></ins>
                        </div>
                        <div class="breadcrumbs">
                            <nav aria-label="breadcrumb">
                                [[pdoCrumbs?
                                &limit=`3`
                                &tpl=`@INLINE <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="[[+link]]">[[+menutitle]]</a></li>`
                                &tplWrapper=`@INLINE <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">[[+output]]</ul>`
                                &tplCurrent=`@INLINE <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">[[+menutitle]]</span></li>`
                                &tplMax=`@INLINE`
                                ]]
                            </nav>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <h1 itemprop="headline">[[!getWayFild? &id=`[[*parent]]` &fild=`pagetitle`]] - [[*pagetitle]]</h1>

                                <div class="introtext">[[*introtext]]</div>
                                <div class="ch-tonality">
                                    <div><button class="btn btn-primary" @click="changeTonality('dec')"> - </button></div>
                                    <div>Тональность {{level}}</div>
                                    <div><button class="btn btn-primary" @click="changeTonality('inc')"> + </button></div>
                                </div>
                                <div class="songwr">
                            <pre class="song"
                                 ref="song">[[*content]]</pre>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="song-accords">
                                    <h2>Аккорды</h2>
                                    <accord-list-comp :accords="accords"></accord-list-comp>
                                </div>
                            </div>
                        </div>
                        [[!If?
                        &subject=`[[*tv_youtube]]`
                        &operator=`!empty`
                        &then=`
                        <h3>Видео</h3>
                        <div class="songvideo">
                            <div class="songvideo_inwr">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/[[*tv_youtube]]?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        `]]

                    </div>
                    [[$sidebar]]
                </div>
            </div>
        </div>
    </main>
</div>
[[$footer]]
<script>
    (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="hitscount hidden">[[!HitsPage? &saveTv=`true`]]</div>
</body>
</html>