<!doctype html>
<html lang="ru">
[[$head]]
<body class="views-tpl">
<div class="glob">
    [[$header]]
    <main role="main">
        <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <h1 itemprop="headline">[[*pagetitle]]</h1>

                    <div class="tablesubcat">
                        <table class="table-striped">
                            <thead>
                            <tr>
                                <th class="td-img-cat"></th>
                                <th colspan="2">Композиция</th>
                                <th class="viewscount">Просмотры</th>
                            </tr>
                            </thead>
                            <tbody>
                            [[!pdoPage?
                            &parents=`0`
                            &includeTVs=`HitsPage`
                            &processTVs=`1`
                            &templates=`11`
                            &sortbyTV=`HitsPage`
                            &sortbyTVType=`integer`
                            &sortdirTV=`DESC`
                            &tpl=`mostViewsList`
                            &tvPrefix=``
                            &maxLimit=`30`
                            &limit=`20`
                            &tplPageActive=`@INLINE <li class="page-item active"><a class="page-link" href="/[[+href]]">[[+pageNo]]</a></li>`
                            &tplPage=`@INLINE <li class="page-item"><a class="page-link" href="/[[+href]]">[[+pageNo]]</a></li>`
                            &tplPageFirst=`@INLINE <li class="page-item control"><a class="page-link" href="/[[+href]]">&lt;&lt;</a></li>`
                            &tplPageLast=`@INLINE <li class="page-item control"><a class="page-link" href="/[[+href]]">&gt;&gt;</a></li>`
                            &tplPagePrev=`@INLINE <li class="page-item control"><a class="page-link" href="/[[+href]]">&lt;</a></li>`
                            &tplPageNext=`@INLINE <li class="page-item control"><a class="page-link" href="/[[+href]]">&gt;</a></li>`
                            &tplPageFirstEmpty=`@INLINE <li class="page-item control"><span class="page-link"> &lt;&lt;</span></li>`
                            &tplPageLastEmpty=`@INLINE <li class="page-item control"><span class="page-link"> &gt;&gt;</span></li>`
                            &tplPagePrevEmpty=`@INLINE <li class="page-item disabled"><span class="page-link"> &lt;</span></li>`
                            &tplPageNextEmpty=`@INLINE <li class="page-item disabled"><span class="page-link"> &gt;</span></li>`
                            ]]

                            </tbody>
                        </table>
                    </div>

                    [[!+page.nav]]
                </div>
                [[$sidebar]]
            </div>
        </div>
        </div>
    </main>
</div>
[[$footer]]
</body>
</html>