<?php
/**
 * Доступные значения:
 * &id - id документа
 * &fild - желаемые данные
 * &uri - по желанию, формирует ссылку с доп значениями:
 *  &title - выводит в title 'pagetitle', 'longtitle' или значение переменной
 *  &class - выводит в class значение переменной
 *  &tag - выводит значение переменной
 */
$page = $modx->getObject('modResource', $id);//значение &id
$output = $page->get($fild);//значение &fild
if ($uri) {//значение &uri
    $uri = $page->get('uri');//получение uri

    $title = ($title == 'pagetitle' || $title == 'longtitle' ?$page->get($title):$title);
    $title = ($title ? ' title="'.$title.'"' : '');//значение &title
    $class = ($class ? ' class="'.$class.'"' : '');//значение &class
    $tag = ($tag ? ' '.$tag : '');//значение &tag
    if($pre_img){
        $pre_img = '<td class="td-img-cat"><div class="imgwr"><a href="'.$uri.'"><img src="' . $page->getTVValue('tv_preimg_subcat') . '" alt="' . $title . '" /></a></div></td>';//получение
        $result = $pre_img . '<td><a href="'.$uri.'"'.$title.$class.$tag.'>' . $output . '</a></td>';//результат
    }else{
        $result = '<a href="'.$uri.'"'.$title.$class.$tag.'>' . $output . '</a>';//результат
    }

    return $result;
} else {
    return $output;
}