import Vue from 'vue';

import SongsListComp from '../components/SongsListComp';


export default class ArticlePage{

    constructor(){

        if(document.getElementById('artistPage')) {

            new Vue({
                el: '#artistPage',
                data: {
                    songs: []
                },
                components: {
                    SongsListComp
                },
                mounted(){

                    let songsData = '[' + this.$refs.songListVueData.innerHTML.trim().slice(0, -1) + ']';
                    this.songs = JSON.parse(songsData);

                }
            });
        }
    }
}