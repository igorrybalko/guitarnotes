import Vue from 'vue';

import ChangeTonality from '../helpers/changeTonality';
import AccordListComp from '../components/AccordListComp';

let changeTonality = new ChangeTonality;

export default class ArticlePage{

    constructor(){

        if(document.getElementById('articlePage')) {

            new Vue({
                el: '#articlePage',
                data: {
                    accords: [],
                    rawAccords: [],
                    level: 0
                },
                mounted(){

                    this.rawAccords = this.$refs.song.getElementsByTagName('b');

                    for (let i = 0 ; i < this.rawAccords.length; i++){
                       this.accords.push(this.rawAccords[i].innerText);
                    }
                },
                methods: {
                    changeTonality(direction){
                        this.accords = [];
                        for (let i = 0 ; i < this.rawAccords.length; i++){
                            this.accords.push(changeTonality[direction + 'Tonality'](this.rawAccords[i].innerText));
                            this.rawAccords[i].innerText = this.accords[i];
                        }
                        this.changeLevel(direction);
                    },
                    changeLevel(direction){

                        let level = this.level;
                        level = parseInt(level);

                        if(direction == 'inc'){

                            if(level == 11){
                                level = -1;
                            }
                            level++;

                        }else{

                            if(level == -11){
                                level = 1;
                            }
                            level--;
                        }

                        if(level > 0){
                            this.level = '+' + level;
                        }else{
                            this.level = level;
                        }
                    }
                },
                components: {
                    AccordListComp
                }
            });

        }
    }

}