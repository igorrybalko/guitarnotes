import ArticlePage from './pages/article-page';
import ArtistPage from './pages/artist-page';

export default class App{

    constructor(){
        new ArticlePage();
        new ArtistPage();
    }
}