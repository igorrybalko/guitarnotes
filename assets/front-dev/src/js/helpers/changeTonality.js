export default class ChangeTonality{

    constructor(){
        this.tonality = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'H'];
        this.currAccords = [];
    }

    getPosition(tone){

        let position;

        switch(tone){
            case 'Db':
                tone = 'C#';
                break;
            case 'Ab':
                tone = 'G#';
                break;
            case 'D#':
                tone = 'Eb';
                break;
            case 'A#':
                tone = 'Bb';
                break;
        }

        position = this.tonality.indexOf(tone);

        if(position > -1){
            return position;
        }
        console.log('error: неудалость конвертировать ' + tone);
        return null;
    }

     defineTone(accord){

        let tone = accord,
            mark = '';

        if(accord.length > 1){
            let firstSymbol = accord.charAt(0);
            let secondSymbol = accord.charAt(1);

            if(secondSymbol == '#' || secondSymbol == 'b'){
                tone = firstSymbol + secondSymbol;
                mark = accord.substr(2);
            }else{
                tone = firstSymbol;
                mark = accord.substr(1);
            }
        }

        return {
            tone,
            mark
        };
     }

    incTonality(props){

        return this.checkComplexAccord(props, 'simpIncTonality');
    }

    decTonality(props){

        return this.checkComplexAccord(props, 'simpDecTonality');
    }

    simpIncTonality(props){

        let accord = this.defineTone(props);
        let position = this.getPosition(accord.tone);

        if(position === 11){
            position = -1;
        }
        position++;

        return this.tonality[position] + accord.mark;
    }

    simpDecTonality(props){

        let accord = this.defineTone(props);
        let position = this.getPosition(accord.tone);

        if(position === 0){
            position = 12;
        }
        position--;

        return this.tonality[position] + accord.mark;
    }

    checkComplexAccord(accord, method){

        let result;

        let isDouble = accord.indexOf('/');
        if(isDouble !== -1){
            let accords =  accord.split('/'),
                mainAccord = this[method](accords[0]),
                basAccord = this[method](accords[1]);
            result = mainAccord + '/' + basAccord;
        }else{
            result = this[method](accord);
        }

        return result
    }
}