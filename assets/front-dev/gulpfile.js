const production = ( process.env.NODE_ENV == 'production' );

//base part
let gulp = require('gulp'),
    rename  = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    webpack  = require('webpack'),
    notifier = require('node-notifier');

//css part
let less = require('gulp-less'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

let webpackConfig = require('./webpack.config.js');
let statsLog      = { // для красивых логов в консоли
    colors: true,
    reasons: true
};

const buildFolder = '../tmpl';

let pathFiles = {
    build : {
        js : './' + buildFolder + '/js/',
        css  : './' + buildFolder + '/css/',
    },
    src : {
        js : './src/js/**/*.*',
        css  : './src/less/**/*.less',
    }
};

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('styles', function() {
    return gulp.src('./src/less/main.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            parth: ['./src/less']
        }))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 5 versions', '> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(pathFiles.build.css));
});

gulp.task('scripts', (done) => {

    function onError(error) {

        console.log(error);
        notifier.notify({ // чисто чтобы сразу узнать об ошибке
            title: `Error: js`,
            message: error
        });

        done();
    }

    function onSuccess(detailInfo) {
        console.log(detailInfo);
        done();
    }

    function onComplete(error, stats) {
        if (error) { // кажется еще не сталкивался с этой ошибкой
            onError(error);
        } else if ( stats.hasErrors() ) { // ошибки в самой сборке, к примеру "не удалось найти модуль по заданному пути"
            onError( stats.toString(statsLog) );
        } else {
            onSuccess( stats.toString(statsLog) );
        }
    }

    // run webpack
    webpack(webpackConfig, onComplete);

});

gulp.task('gulp_watch', function () {
    gulp.watch(pathFiles.src.css, gulp.series('styles'));
    gulp.watch(pathFiles.src.js, gulp.series('scripts'));
});

gulp.task('build', gulp.series('styles', 'scripts'));

let tasks = gulp.series('build', 'gulp_watch');

if(production){
    tasks = gulp.series('build');
}

gulp.task('default', tasks);